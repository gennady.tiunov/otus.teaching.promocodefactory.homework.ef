﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DataContext(DbContextOptions<DataContext> dbContextOptions) : base(dbContextOptions)
        {
            Database.EnsureDeleted();
            //Database.EnsureCreated();
            Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new PreferenceConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerPreferenceConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeeConfiguration());

            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            //modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);

            modelBuilder.Entity<CustomerPreference>().HasData(
            new
            {
                CustomerId =  Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c")
            });
        }

        public class RoleConfiguration : IEntityTypeConfiguration<Role>
        {
            public void Configure(EntityTypeBuilder<Role> builder)
            {
                builder.ToTable("Role").HasKey(p => p.Id);
                builder.Property(p => p.Name).IsRequired().HasMaxLength(50);
            }
        }

        public class PreferenceConfiguration : IEntityTypeConfiguration<Preference>
        {
            public void Configure(EntityTypeBuilder<Preference> builder)
            {
                builder.ToTable("Preference").HasKey(p => p.Id);
                builder.Property(p => p.Name).IsRequired().HasMaxLength(50);
            }
        }

        public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
        {
            public void Configure(EntityTypeBuilder<Customer> builder)
            {
                builder.ToTable("Customer").HasKey(p => p.Id);
                builder.Property(p => p.FirstName).IsRequired().HasMaxLength(30);
                builder.Property(p => p.LastName).IsRequired().HasMaxLength(50);
                builder.Property(p => p.Email).IsRequired().HasMaxLength(100);
                builder.HasMany(p => p.PromoCodes).WithOne(p => p.Customer);
            }
        }

        public class CustomerPreferenceConfiguration : IEntityTypeConfiguration<CustomerPreference>
        {
            public void Configure(EntityTypeBuilder<CustomerPreference> builder)
            {
                builder.ToTable("CustomerPreference").HasKey(cp => new { cp.CustomerId, cp.PreferenceId });

                builder.HasOne(cp => cp.Customer).WithMany(c => c.CustomerPreferences).HasForeignKey(cp => cp.CustomerId).OnDelete(DeleteBehavior.Cascade);
                builder.HasOne(cp => cp.Preference).WithMany(c => c.CustomerPreferences).HasForeignKey(cp => cp.PreferenceId).OnDelete(DeleteBehavior.Cascade);
            }
        }

        public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
        {
            public void Configure(EntityTypeBuilder<Employee> builder)
            {
                builder.ToTable("Employee").HasKey(p => p.Id);
                builder.Property(p => p.FirstName).IsRequired().HasMaxLength(30);
                builder.Property(p => p.LastName).IsRequired().HasMaxLength(50);
                builder.Property(p => p.Email).IsRequired().HasMaxLength(100);
                builder.HasOne(p => p.Role);
                builder.HasMany(p => p.PromoCodes).WithOne(p => p.PartnerManager).OnDelete(DeleteBehavior.Cascade);
            }
        }

        public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
        {
            public void Configure(EntityTypeBuilder<PromoCode> builder)
            {
                builder.ToTable("PromoCode").HasKey(p => p.Id);
                builder.Property(p => p.Code).IsRequired().HasMaxLength(30);
                builder.Property(p => p.ServiceInfo).IsRequired().HasMaxLength(100);
                builder.Property(p => p.PartnerName).IsRequired().HasMaxLength(100);
                builder.HasOne(p => p.PartnerManager).WithMany(p => p.PromoCodes);
                builder.HasOne(p => p.Preference);
                builder.HasOne(p => p.Customer).WithMany(p => p.PromoCodes).OnDelete(DeleteBehavior.Cascade);
            }
        }
    }
}
