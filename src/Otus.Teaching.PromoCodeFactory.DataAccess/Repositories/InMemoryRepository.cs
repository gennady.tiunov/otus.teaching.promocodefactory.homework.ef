﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private IEnumerable<T> _data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            _data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(_data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(_data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddAsync(T entity)
        {
            entity.Id = Guid.NewGuid();

            _data = _data.Append(entity);

            return Task.FromResult(entity.Id);
        }

        public Task UpdateAsync(T entity)
        {
            var elements = _data.ToDictionary(p => p.Id);

            elements[entity.Id] = entity;

            _data = elements.Values;

            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            _data = _data.Where(p => p.Id != id);

            return Task.CompletedTask;
        }
    }
}