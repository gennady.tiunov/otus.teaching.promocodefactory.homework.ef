﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;
        
        public PromoCodesController(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<Customer> customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromoCodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            var promoCodeModels = promoCodes.Select(p =>
                new PromoCodeShortResponse
                {
                    Id = p.Id,
                    Code = p.Code,
                    ServiceInfo = p.ServiceInfo,
                    BeginDate = p.BeginDate.ToString(),
                    EndDate = p.EndDate.ToString(),
                    PartnerName = p.PartnerName,
                }).ToList();

            return Ok(promoCodeModels);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();
            if (promoCodes.Any(p => p.Code.Equals(request.PromoCode, StringComparison.InvariantCultureIgnoreCase)))
            {
                return Conflict();
            }

            var preferences  = await _preferenceRepository.GetAllAsync();
            var preference = preferences.FirstOrDefault(p => p.Name.Equals(request.Preference, StringComparison.InvariantCultureIgnoreCase));
            if (preference == null)
            {
                return BadRequest();
            }

            var customers = await _customerRepository.GetAllAsync();
            var customer = customers.FirstOrDefault(c => c.CustomerPreferences.Any(p => p.Preference.Name.Equals(request.Preference, StringComparison.InvariantCultureIgnoreCase)));
            if (customer == null)
            {
                return BadRequest();
            }

            var promoCode = new PromoCode
            {
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                BeginDate = DateTime.UtcNow,
                EndDate = DateTime.MaxValue,
                Preference = preference,
                Customer = customer
            };

            await _promoCodeRepository.AddAsync(promoCode);

            var url = $"{ControllerContext.HttpContext.Request.Scheme}://{ControllerContext.HttpContext.Request.Host}/api/v1/PromoCodes/{promoCode.Id}";

            return Created(url, promoCode.Id);
        }
    }
}