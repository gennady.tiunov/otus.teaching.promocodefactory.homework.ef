﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public CustomersController(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<PromoCode> promoCodeRepository
            )
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customerModels = customers.Select(c =>
                new CustomerShortResponse
                {
                    Id = c.Id,
                    FirstName = c.FirstName,
                    LastName = c.LastName,
                    Email = c.Email                    
                }).ToList();

            return Ok(customerModels);
        }

        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            var customerModel = 
                new CustomerResponse
                {
                  Id = customer.Id,
                  FirstName = customer.FirstName,
                  LastName = customer.LastName,
                  Email = customer.Email,
                  Preferences = customer.CustomerPreferences.Select(p =>
                  new PreferenceResponse
                  {
                      Id = p.Preference.Id,
                      Name = p.Preference.Name
                  }).ToList(),
                  PromoCodes = customer.PromoCodes.Select(c =>
                  new PromoCodeShortResponse
                  {
                      Id = c.Id,
                      Code = c.Code,
                      ServiceInfo = c.ServiceInfo,
                      BeginDate = c.BeginDate.ToString(),
                      EndDate = c.EndDate.ToString(),
                      PartnerName = c.PartnerName,
                  })
                  .ToList()
              };

            return Ok(customerModel);
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences  = (await _preferenceRepository.GetAllAsync()).ToList();

            var preferenceIds = request.PreferenceIds?? new List<Guid>();
            foreach (var preferenceId in preferenceIds)
            {
                if (preferences.All(p => p.Id != preferenceId))
                    return BadRequest();
            }

            var customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            await _customerRepository.AddAsync(customer);

            customer.CustomerPreferences = preferences
                .Where(p => preferenceIds.Contains(p.Id))
                .Select(p => 
                    new CustomerPreference 
                    {
                        CustomerId = customer.Id,
                        PreferenceId = p.Id
                    }).ToList();

            await _customerRepository.UpdateAsync(customer);

            var url = $"{ControllerContext.HttpContext.Request.Scheme}://{ControllerContext.HttpContext.Request.Host}/api/v1/Employees/{customer.Id}";

            return Created(url, customer.Id);
        }

        /// <summary>
        /// Обновить данные клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var preferences = (await _preferenceRepository.GetAllAsync()).ToList();

            var preferenceIds = request.PreferenceIds ?? new List<Guid>();
            foreach (var preferenceId in preferenceIds)
            {
                if (preferences.All(p => p.Id != preferenceId))
                    return BadRequest();
            }

            customer.Id = id;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.CustomerPreferences = preferences.Where(p => preferenceIds.Contains(p.Id))
                .Select(p =>
                    new CustomerPreference
                    {
                        CustomerId = customer.Id,
                        PreferenceId = p.Id
                    }).ToList();

            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            await _customerRepository.DeleteAsync(id);

            foreach (var promoCode in customer.PromoCodes)
            {
                await _promoCodeRepository.DeleteAsync(promoCode.Id);
            }

            return NoContent();
        }
    }
}